# Contributor: Díaz Urbaneja Diego <sodomon2@gmail.com>
# Maintainer: Díaz Urbaneja Diego <sodomon2@gmail.com>
pkgname=ppsspp
pkgver=1.14.3
pkgrel=0
pkgdesc="PPSSPP - a fast and portable PSP emulator"
url="https://www.ppsspp.org/"
arch="aarch64 x86 x86_64 ppc64le" # other arches fail to build
license="GPL-2.0-only"
makedepends="
	cmake
	ffmpeg4-dev
	glew-dev
	libzip-dev
	mesa-dev
	miniupnpc-dev
	python3
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	samurai
	sdl2-dev
	snappy-dev
	zlib-dev
	zstd-dev
	"
source="https://github.com/hrydgard/ppsspp/releases/download/v$pkgver/ppsspp-$pkgver.tar.xz"
options="!check" # make check not implemented

build() {
	cmake -B build-qt -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DUSE_SYSTEM_FFMPEG=ON \
		-DUSE_SYSTEM_LIBPNG=ON \
		-DUSE_SYSTEM_LIBSDL2=ON \
		-DUSE_SYSTEM_LIBZIP=ON \
		-DUSE_SYSTEM_MINIUPNPC=ON \
		-DUSE_SYSTEM_SNAPPY=ON \
		-DUSE_SYSTEM_ZSTD=ON \
		-DUSING_QT_UI=ON \
		-DUSING_GLES2=ON \
		-DUSING_EGL=ON
	cmake --build build-qt
}

package() {
	DESTDIR="$pkgdir" cmake --install build-qt
}


sha512sums="
7c61523245a6b8a2e793123864a7758aca230ff76ee6510d71fcd65fc71b71aad48567ae6deb8ac9c4c4a624fb53dc99a68fc70c8ce843d5428eae39d2d87ba0  ppsspp-1.14.3.tar.xz
"
