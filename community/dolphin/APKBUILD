# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=dolphin
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/system/dolphin"
pkgdesc="KDE File Manager"
license="GPL-2.0-only"
depends="
	udisks2
	kio-extras
	"
depends_dev="
	baloo-dev
	baloo-widgets-dev
	kactivities-dev
	kbookmarks-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kparts-dev
	ktextwidgets-dev
	kuserfeedback-dev
	kwindowsystem-dev
	phonon-dev
	qt5-qtbase-dev
	samurai
	solid-dev
	"
makedepends="$depends_dev extra-cmake-modules ruby-test-unit"
checkdepends="
	xvfb-run
	dbus
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/dolphin-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# kfileitemmodeltest, placesitemmodeltest and dolphinmainwindowtest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(kfileitemmodel|placesitemmodel|dolphinmainwindow)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
051dde0f79839cd1e34fe41536fd4a78ce336e0c2d4622da6cd213f3daccd9c0112751d75429f3f7bdce606bdba66a3fd34b5bcc3c80e6297051622ce3f454ad  dolphin-22.12.0.tar.xz
"
