# Contributor: Alex Yam <alex@alexyam.com>
# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Alex Yam <alex@alexyam.com>
pkgname=libplacebo
pkgver=5.229.1
pkgrel=2
pkgdesc="Reusable library for GPU-accelerated video/image rendering"
url="https://code.videolan.org/videolan/libplacebo"
arch="all"
license="LGPL-2.1-or-later"
makedepends="
	glfw-dev
	glslang-dev
	glslang-static
	lcms2-dev
	libepoxy-dev
	meson
	py3-glad
	py3-mako
	sdl2-dev
	sdl2_image-dev
	shaderc-dev
	spirv-tools-dev
	vulkan-loader-dev
	"
subpackages="$pkgname-dev"
source="https://code.videolan.org/videolan/libplacebo/-/archive/v$pkgver/libplacebo-v$pkgver.tar.gz
	$pkgname-colorspace-1.patch::https://github.com/haasn/libplacebo/commit/7378526770b823cf76ca66d31ad6aa079dd15cec.patch
	$pkgname-colorspace-2.patch::https://github.com/haasn/libplacebo/commit/9e9b93502d984900cecde793d884c6c79c4cafc1.patch
	$pkgname-renderer.patch::https://github.com/haasn/libplacebo/commit/7ead30db8aa75db28236eee40899f0ff6e3c9688.patch
	"
builddir="$srcdir/libplacebo-v$pkgver"

build() {
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	CPPFLAGS="$CPPFLAGS -O2" \
	abuild-meson \
		-Db_lto=true \
		-Ddemos=false \
		-Dglslang=enabled \
		-Dlcms=enabled \
		-Dshaderc=enabled \
		-Dtests=true \
		-Dvulkan=enabled \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
45efb252d5d44e5429790f9024b1b2a153051d6d604b07fcf3e4de7d49a2e91d20457be76e08b3b9ab43248c4a2e7268c9f8dc69837783e572f9b8735c955334  libplacebo-v5.229.1.tar.gz
14fa06fc1f5a606ace77d9ba895e795714150eeeff19f46c440b727d0ea858ed0215f3e50ce2a53b04ccad90336b77dc1202148bf406d416f3f2e8b722a11b1b  libplacebo-colorspace-1.patch
2bf20066956435ce3b39ecdfa57e159e3e981775566fb7eeb01bb5c2772e9b273903c35e54e3ca754c446d7854db3d7b8e67794405c4441b55f1ac422b3178dc  libplacebo-colorspace-2.patch
4b2ad8b16127844f86062d8d6585ca9c9d961b4d0e815b6946de688de497442d3247338c0a11ff5528b6083d6cdf5bd099f83f739f7c149e284f25b9fca95df8  libplacebo-renderer.patch
"
