# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdesdk-kio
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development"
pkgdesc="KIO-Slaves"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kio-dev
	perl-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdesdk-kio-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
14e7adecf22ffa741c55c1edaae3caa47c0bd24c8b618dfbff798093725a19a27dfea659408626ac8339df9ae4c6a56f0c08e8c74729cfc60bf046d1cc48d27f  kdesdk-kio-22.12.0.tar.xz
"
