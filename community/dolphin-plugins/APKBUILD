# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=dolphin-plugins
pkgver=22.12.0
pkgrel=0
pkgdesc="Extra Dolphin plugins"
url="https://www.kde.org/applications/system/dolphin/"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later"
makedepends="
	dolphin-dev
	extra-cmake-modules
	ki18n-dev
	kio-dev
	ktexteditor-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/dolphin-plugins-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_svn=OFF # Broken "error: expected unqualified-id before '(' token"
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
be257382b493c2f318cfba7c594a1ef89c358ba5ccb3ff7478313040923f319106a1f94fcf9699695d3d17b59031bcdc88352fc6c58f779475b41a5080dc2126  dolphin-plugins-22.12.0.tar.xz
"
