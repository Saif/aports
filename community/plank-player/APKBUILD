# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plank-player
pkgver=5.26.4
pkgrel=0
pkgdesc="Multimedia Player for playing local files on Plasma Bigscreen"
url="https://invent.kde.org/plasma-bigscreen/plank-player"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later"
depends="
	kirigami2
	qt5-qtgraphicaleffects
	qt5-qtmultimedia
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami2-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
source="https://download.kde.org/stable/plasma/$pkgver/plank-player-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4bc5c7294706062f38e57d7365bcaeb340147a0bd6fbb5dbf8f292684e11b5c3cce11cb299ec6f00f9211d36a721cef9d8af0d5044e8fdf160529874eed153b7  plank-player-5.26.4.tar.xz
"
