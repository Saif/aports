# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kbackup
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kbackup"
pkgdesc="An application which lets you back up your data in a simple, user friendly way"
license="GPL-2.0-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kbackup-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fd4de3601a790e310313c41dbe3591549e175049ba06062f57903826c0d2f0ba004e6ccee260761b28133d8152c3c9201c3f1a6e3f648aabee2eaa4bda3f5615  kbackup-22.12.0.tar.xz
"
