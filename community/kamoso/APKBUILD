# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kamoso
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> purpose
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/multimedia/org.kde.kamoso"
pkgdesc="An application to take pictures and videos out of your webcam"
license="GPL-2.0-or-later AND LGPL-2.1-only"
depends="
	gst-plugins-bad
	gst-plugins-good
	kirigami2
	qt5-qtquickcontrols2
	"
makedepends="
	extra-cmake-modules
	glib-dev
	gobject-introspection-dev
	gst-plugins-base-dev
	gstreamer-dev
	kconfig-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	purpose-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kamoso-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3b2ed7a00ae6bc217abc7ba7ddcb14bc9c9d1972f00d61a17359d5444d5db1a993cf3f1f465949c7d463fe138567c270fe2837b9cfd64701e3b50f0f9e4c97f3  kamoso-22.12.0.tar.xz
"
