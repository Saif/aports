# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=evolution-ews
pkgver=3.46.2
pkgrel=0
pkgdesc="MS Exchange integration through Exchange Web Services"
url="https://wiki.gnome.org/Apps/Evolution/EWS"
arch="all !s390x !riscv64" # blocked by evolution
license="LGPL-2.1-or-later"
makedepends="cmake gtk-doc intltool glib-dev gettext-dev evolution-data-server-dev
	evolution-dev libmspack-dev uhttpmock-dev libsoup3-dev samurai"
options="!check" # fail in docker due to port restrictions
subpackages="$pkgname-lang"
source="https://download.gnome.org/sources/evolution-ews/${pkgver%.*}/evolution-ews-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DENABLE_TESTS=OFF
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dd669b8d562849d588f88dd40f981e0401a046cb5489b00894082b04462fadc43581dadb0eee87077b63998a32e80cb66ae9a5c0de99c7079bb5cbc5aa8c37bd  evolution-ews-3.46.2.tar.xz
"
