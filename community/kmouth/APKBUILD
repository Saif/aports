# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmouth
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kmouth"
pkgdesc="Speech Synthesizer Frontend"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtspeech-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmouth-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKF_IGNORE_PLATFORM_CHECK=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d4f9e6bf47fb6b608c4736f92fd00d6ec4ab154f6742cc2fc9f5734bf986cdf457ea9e4e6de7d964ffd2ca5d322aceacf6f370d72196e51138267392d7c0382a  kmouth-22.12.0.tar.xz
"
