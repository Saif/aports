# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kirigami-gallery
pkgver=22.12.0
pkgrel=0
arch="all !armhf" # armhf blocked by kirigami2 -> qt5-qtdeclarative
url="https://kde.org/applications/development/org.kde.kirigami2.gallery"
pkgdesc="Gallery application built using Kirigami"
license="LGPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kirigami2-dev
	kitemmodels-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kirigami-gallery-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5b1bc64f45df12680eae3499c927108c0e402db0ca7d0bec8b09a780957cf7bb2f22ce3827f017aeb4f884122a2eed078f2ee66dd4004e7608fc4c8cc9a63f6a  kirigami-gallery-22.12.0.tar.xz
"
