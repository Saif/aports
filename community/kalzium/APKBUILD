# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalzium
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/kalzium/"
pkgdesc="Periodic Table of Elements"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	eigen-dev
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	knewstuff-dev
	kparts-dev
	kplotting-dev
	kunitconversion-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	solid-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalzium-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
293fbb4c105834acde1dfa16bdad59633b88fc0c0e064d4236560490fcc1c4323d5a380bac6ca408b8b5782e7ed26b3e1ec7733bc406eeeaa6c765673e4dd36b  kalzium-22.12.0.tar.xz
"
