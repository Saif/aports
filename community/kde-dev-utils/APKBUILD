# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-dev-utils
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development/"
pkgdesc="Small utilities for developers using KDE/Qt libs/frameworks"
license="(LGPL-2.0-only OR LGPL-3.0-only) AND GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	ki18n-dev
	kparts-dev
	kwidgetsaddons-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kde-dev-utils-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
966c323f85dc2f3c8965d6bcf07f885bf536f7d8fcfb5a420f033e4e0f01765c1b8a71f3db320702eea606dc60215534736401ec8d1eeb6be4c4eca38c730233  kde-dev-utils-22.12.0.tar.xz
"
