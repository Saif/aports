# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-tempora
pkgver=5.1.0
pkgrel=0
pkgdesc="Objects and routines pertaining to date and time (tempora)"
url="https://github.com/jaraco/tempora"
arch="noarch"
license="MIT"
depends="
	py3-jaraco.functools
	py3-tz
	python3
	"
makedepends="py3-gpep517 py3-setuptools_scm py3-wheel"
checkdepends="py3-pytest py3-freezegun py3-pytest-freezegun"
source="https://pypi.python.org/packages/source/t/tempora/tempora-$pkgver.tar.gz"
builddir="$srcdir/tempora-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION="$pkgver"
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" dist/tempora-$pkgver-py3-none-any.whl
}

sha512sums="
33b48884c024f82e3220c2cc28a307bbb3c813a895df82640b5627266355c78cced3e5ab61df9b0619bf9697e18d70390939189ba31dd57c58fbb3e97b5db216  tempora-5.1.0.tar.gz
"
