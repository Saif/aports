# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kanagram
pkgver=22.12.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://edu.kde.org/kanagram/"
pkgdesc="Letter Order Game"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	libkeduvocdocument-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	sonnet-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kanagram-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
64e7c5d63f9fd422ed5fdbbcc41edc3a47007f7eebd01621a6a9446320fe461d41b5bec754fc64a35ff435812893d180d091490a4d25cf11c9ec852120fd8170  kanagram-22.12.0.tar.xz
"
