# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=krdc
pkgver=22.12.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/internet/krdc/"
pkgdesc="Remote Desktop Client"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="freerdp"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	knotifications-dev
	knotifyconfig-dev
	knotifyconfig-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libssh-dev
	libvncserver-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/krdc-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6819adc4c85050480a2c594d249cf19474a31b01c52381692a2a968ad1e4f320ef291cd8ef04b59623129023a8bc2e727572bea22ed8aed7a565bfc0cef57e81  krdc-22.12.0.tar.xz
"
