# Maintainer: psykose <alice@ayaya.dev>
pkgname=hwdata
pkgver=0.365
pkgrel=0
pkgdesc="Hardware identification and configuration data"
url="https://github.com/vcrhonek/hwdata"
arch="noarch"
license="GPL-2.0-or-later"
subpackages="$pkgname-dev $pkgname-usb $pkgname-pci $pkgname-pnp $pkgname-net"
source="$pkgname-$pkgver.tar.gz::https://github.com/vcrhonek/hwdata/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # just firmware data

replaces="hwids"
# be higher since hwids was a large date version
provides="hwids=20220101-r$pkgrel"

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--datadir=/usr/share \
		--disable-blacklist
}

package() {
	depends="$pkgname-usb $pkgname-pci $pkgname-pnp $pkgname-net"
	make -j1 DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	depends="$pkgname=$pkgver-r$pkgrel"
}

usb() {
	pkgdesc="$pkgdesc (usb data)"
	provides="hwids-usb=20220101-r$pkgrel"
	replaces="hwids-usb"

	amove usr/share/hwdata/usb.ids
}

pci() {
	pkgdesc="$pkgdesc (pci data)"
	provides="hwids-pci=20220101-r$pkgrel"
	replaces="hwids-pci"

	amove usr/share/hwdata/pci.ids
}

net() {
	pkgdesc="$pkgdesc (net data)"
	provides="hwids-net=20220101-r$pkgrel"
	replaces="hwids-net"

	amove usr/share/hwdata/oui.txt
	amove usr/share/hwdata/iab.txt
}

pnp() {
	pkgdesc="$pkgdesc (pnp data)"

	amove usr/share/hwdata/pnp.ids
}


sha512sums="
b9f8b144ab2cf8edcd585efe5d318449e8243310d0106fa830cbb621cfe53efc5aabc6b8795b8aefba62b4771e6c22ab5742099de2be5c9ca2cf132aac6e9c61  hwdata-0.365.tar.gz
"
