# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-sphinx
pkgver=6.0.0
pkgrel=0
pkgdesc="Python Documentation Generator"
# checkdepends require 'imagemagick' and 'py3-html5lib' which
# are in community/, 'imagemagick' itself also needs 'librsvg'
# and 'libheif', which bring in 'rust', 'x265' and others, this
# is too much of a burden to put on main/
options="!check"
url="https://www.sphinx-doc.org/"
arch="noarch"
license="BSD-2-Clause"
depends="
	py3-babel
	py3-docutils
	py3-imagesize
	py3-jinja2
	py3-packaging
	py3-pygments
	py3-requests
	py3-snowballstemmer
	py3-alabaster
	py3-sphinxcontrib-applehelp
	py3-sphinxcontrib-devhelp
	py3-sphinxcontrib-htmlhelp
	py3-sphinxcontrib-jsmath
	py3-sphinxcontrib-serializinghtml
	py3-sphinxcontrib-qthelp
	"
makedepends="py3-gpep517 py3-flit-core py3-installer"
# imagemagick is for tests/test_ext_imgconverter.py::test_ext_imgconverter
# which calls the 'convert' binary
checkdepends="py3-pytest py3-funcsigs py3-pluggy imagemagick py3-html5lib"
source="$pkgname-$pkgver.tar.gz::https://github.com/sphinx-doc/sphinx/archive/v$pkgver.tar.gz"
builddir="$srcdir/sphinx-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	make PYTHON=python3 test
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/sphinx-*.whl
}

sha512sums="
6efcbe57170cacec6f3c7f11f09094d096dc2afb7e0470c7cbdb9ea209dcbb782a6227a32de41974258555fa01986406a6564587f6b414594c452dcc6b8a06fc  py3-sphinx-6.0.0.tar.gz
"
